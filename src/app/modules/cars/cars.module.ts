import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CarsTableComponent } from './cars-table/cars-table.component';

// STORE
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { carReducer } from 'src/app/shared/stores/cars/cars.reducer';
import { CarsEffect } from 'src/app/shared/stores/cars/cars.effect';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CarsAddFormComponent } from './cars-add-form/cars-add-form.component';
import { CarsEditFormComponent } from './cars-edit-form/cars-edit-form.component';


const routes: Routes = [
  { path: "", component: CarsTableComponent },
  { path: "add", component: CarsAddFormComponent },
  { path: "edit/:id", component: CarsEditFormComponent },
];

@NgModule({
  declarations: [
    CarsTableComponent,
    CarsAddFormComponent,
    CarsEditFormComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature('mycars', carReducer),
    EffectsModule.forFeature([CarsEffect])
  ]
})
export class CarsModule { }