import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarsEditFormComponent } from './cars-edit-form.component';

describe('CarsEditFormComponent', () => {
  let component: CarsEditFormComponent;
  let fixture: ComponentFixture<CarsEditFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CarsEditFormComponent]
    });
    fixture = TestBed.createComponent(CarsEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
