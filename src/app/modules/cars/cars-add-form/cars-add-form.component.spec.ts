import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarsAddFormComponent } from './cars-add-form.component';

describe('CarsAddFormComponent', () => {
  let component: CarsAddFormComponent;
  let fixture: ComponentFixture<CarsAddFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CarsAddFormComponent]
    });
    fixture = TestBed.createComponent(CarsAddFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
